#	Marblibre, a free software 3D marble game
#	Copyright (C) 2017 retrotails retrotails@yahoo.com

#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, specifically version 3 of the License.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends VBoxContainer

func _ready():
	set_process_input(true)

func _on_play_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().set_pause(false)
	get_node("../pausemenu").hide()

func _on_quit_pressed():
	get_tree().quit()

func _input(event):
	# Unpause
	if Input.is_action_pressed("ui_pause") && !event.is_echo():
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		get_tree().set_pause(false)
		self.hide()