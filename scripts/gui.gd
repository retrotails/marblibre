#	Marblibre, a free software 3D marble game
#	Copyright (C) 2017 retrotails retrotails@yahoo.com

#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, specifically version 3 of the License.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends Spatial

# If you press the pause button to unpause, it'll register the button again and keep the game paused...
var wasJustPaused = 0.5

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_input(true)
	set_fixed_process(true)
	pass

func _fixed_process(dt):
	if wasJustPaused > 0:
		wasJustPaused -= dt

func _input(event):
	# Pause game
	if Input.is_action_pressed("ui_pause") && !event.is_echo():
		# SFX are now paused in the player node
		#get_node("player/marble/slide").set_volume(sfxid_slide, 0)
		#get_node("player/marble/wind").set_volume(sfxid_wind, 0)
		#get_node("player/marble/roll").set_volume(sfxid_roll, 0)
		wasJustPaused = 0.5
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().set_pause(true)
		get_node("gui/pausemenu").show()
