#	Marblibre, a free software 3D marble game
#	Copyright (C) 2017 retrotails retrotails@yahoo.com

#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, specifically version 3 of the License.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends Spatial

var camera_offset = Vector3(0, 1, 4)
var camera_rot = 3.1415*0.5

var last_vel = 0
var sfxid_slide = 0
var sfxid_wind = 0
var sfxid_roll = 0
var timer = 0
var jumped = false
var jump_cooldown = 0.2

func _ready():
	# Set marble texture to viewport render
	get_node("marble/mesh").get_material_override().set_shader_param("reflection", get_node("marble/view_reflect").get_render_target_texture())
	get_node("marble/mesh").get_material_override().set_shader_param("refraction", get_node("marble/view_refract").get_render_target_texture())

	set_process(true)
	set_fixed_process(true)
	set_process_input(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	# TODO make this not play constantly
	sfxid_slide = get_node("marble/slide").play("slide")
	sfxid_wind = get_node("marble/wind").play("wind")
	sfxid_roll = get_node("marble/roll").play("roll")

func _input(event):
	var marble = get_node("marble")
	var ray = get_node("marble/ray")

	if event.type == InputEvent.MOUSE_MOTION:
		camera_rot += event.relative_x * 0.0025
	# Stop SFX
	if Input.is_action_pressed("ui_pause"):
		get_node("marble/slide").set_volume(sfxid_slide, 0)
		get_node("marble/wind").set_volume(sfxid_wind, 0)
		get_node("marble/roll").set_volume(sfxid_roll, 0)

	# Reset
	if Input.is_action_pressed("reset"):
		get_tree().change_scene("res://levels/test/main.tscn")

func _process(delta):
	var camera = get_node("Camera")
	var marble = get_node("marble")
	var cam_reflect = marble.get_node("view_reflect/camera")
	var cam_refract = marble.get_node("view_refract/camera")

	# Analog camera
	# TODO make this not a hardcoded axis
	if abs(Input.get_joy_axis(0, 2)) > 0.1:
		camera_rot += Input.get_joy_axis(0, 2)*0.1

	if Input.is_action_pressed("camera_left"):
		camera_rot -= delta*2
	if Input.is_action_pressed("camera_right"):
		camera_rot += delta*2

	# Set viewport cameras to marble position
	cam_reflect.set_global_transform(marble.get_global_transform())
	cam_refract.set_global_transform(marble.get_global_transform())

	# Fix game camera to marble, and rotate
	camera.set_translation(Vector3(marble.get_translation().x + camera_offset.z*cos(camera_rot),camera_offset.y + marble.get_translation().y,marble.get_translation().z + camera_offset.z*sin(camera_rot)))
	camera.set_rotation(Vector3(0, -camera_rot + 3.1415*0.5, 0))

	#camera.set_translation(Vector3(camera.get_translation().x + get_viewport().get_mouse_pos().x*0.01 - 0.5, camera.get_translation().y + get_viewport().get_mouse_pos().y*-0.01 + 0.5, camera.get_translation().z))

	# Set viewport cameras to face game camera, or away in case of refraction
	cam_reflect.look_at(camera.get_translation(), Vector3(0, 1, 0))
	var cam = camera.get_translation()
	var mar = marble.get_translation()
	cam_refract.look_at(mar + (mar - cam), Vector3(0, 1, 0))

	#cam_refract.set_translation(Vector3(marble.get_translation().x - camera_offset.z*cos(-camera_rot),-camera_offset.y + marble.get_translation().y,marble.get_translation().z - camera_offset.z*sin(-camera_rot)))
	#x = cx + r * cos(a)
	#y = cy + r * sin(a)
	#camera.set_translation(marble.get_translation() + camera_offset)

func _fixed_process(delta):
	var marble = get_node("marble")
	var colliding = marble.get_node("area").get_overlapping_bodies().size() > 1

	# Jump
	# If we jumped last frame
	if typeof(jumped) == TYPE_VECTOR3:
		marble.apply_impulse(Vector3(0,0,0), (marble.get_global_transform().origin - jumped).normalized()*4)
		jumped = false
	if Input.is_action_pressed("jump") and jump_cooldown == 0 and colliding:
		get_node("marble/col").set_scale(Vector3(2,2,2))
		jumped = marble.get_global_transform().origin
		jump_cooldown = 0.2
	else:
		get_node("marble/col").set_scale(Vector3(1,1,1))
	jump_cooldown = max(jump_cooldown - delta, 0)

	# SFX
	if abs(last_vel - marble.get_linear_velocity().length())*delta > 0.005:
		var id = get_node("marble/hit").play("hit")
		get_node("marble/hit").set_volume(id, abs(last_vel - marble.get_linear_velocity().length())*delta*10)
	last_vel = marble.get_linear_velocity().length()
	#print(abs(last_vel - marble.get_linear_velocity().length())*delta*0.00001)

	# Slide squeaking
	if colliding:
		if ((marble.get_angular_velocity().length()*0.5 - marble.get_linear_velocity().length())*0.05 - 0.1) > 0:
			get_node("marble/slide").set_volume(sfxid_slide, (marble.get_angular_velocity().length()*0.5 - marble.get_linear_velocity().length())*0.05 - 0.1)
			get_node("marble/slide").set_pitch_scale(sfxid_slide, 0.5 + (marble.get_angular_velocity().length() - marble.get_linear_velocity().length())*0.05)
			#get_node("marble/roll").set_volume(sfxid_slide, marble.get_linear_velocity().length()*4)
			get_node("marble/roll").set_pitch_scale(sfxid_slide, marble.get_linear_velocity().length()*0.025 - 0.025 + 0.5)
		else:
			get_node("marble/roll").set_volume(sfxid_slide, marble.get_linear_velocity().length()*0.01 - 0.025)
			get_node("marble/roll").set_pitch_scale(sfxid_slide, marble.get_linear_velocity().length()*0.025 - 0.025 + 0.5)
			get_node("marble/slide").set_volume(sfxid_slide, 0)
	else:
		get_node("marble/slide").set_volume(sfxid_slide, 0)
		get_node("marble/roll").set_volume(sfxid_roll, 0)

	get_node("marble/wind").set_volume(sfxid_wind, marble.get_linear_velocity().length()*0.01 - 0.05)

	# Analog movement
	# TODO make this not a hardcoded axis
	# TODO make braking stronger than accelerating
	var anal = Vector2(clamp(Input.get_joy_axis(0,0), -1, 1), clamp(Input.get_joy_axis(0,1), -1, 1))
	# Deadzone
	if anal.length() < 0.1:
		anal = Vector2(0,0)
	var keys = [Input.is_action_pressed("move_forward"), Input.is_action_pressed("move_back"), Input.is_action_pressed("move_left"), Input.is_action_pressed("move_right")]
	# Digital movement
	if keys[0] or keys[1] or keys[2] or keys[3]:
		var x = 0
		var y = 0
		if keys[0] and not keys[1]:
			y = -1
		if keys[1] and not keys[0]:
			y = 1
		if keys[2] and not keys[3]:
			x = -1
		if keys[3] and not keys[2]:
			x = 1
		anal = Vector2(x,y).normalized()


	# Get the difference between where you're trying to point and where the marble velocity is
	# The more you're trying to fight with the current way it's going, the more velocity we apply
	# Brakes are stronger than acceleration
	# This only applies to X and Z, we ignore the Y axis
	#var marble_vel = Vector2(marble.get_angular_velocity().x, marble.get_angular_velocity().z)
	#var brakes = atan2(sin(anal.angle() - marble_vel.angle()), cos(anal.angle()-marble_vel.angle())) + 3.14159

	# Rotational velocity is the majority of movement
	marble.set_angular_velocity( marble.get_angular_velocity() +
		Vector3(
			0, 0, delta*anal.length()*64
		).rotated(
			Vector3(0, 1, 0),
			camera_rot - anal.angle() + 3.1415
		)
	)
	# Forcibly moves you a bit, allows a bit of movement mid-air
	marble.apply_impulse(Vector3(0, 0, 0), Vector3(0, 0, delta*anal.length()).rotated(Vector3(0, 1, 0)*2, camera_rot - anal.angle() - 3.1415*0.5))